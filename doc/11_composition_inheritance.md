## so sánh Composition vs Inheritance => sắp đặt hay thừa kế

* containment chưa đựng
* specialization chuyên môn hóa 

``` javascript
function FancyBorder(props) {
  return (
    <div className={'FancyBorder FancyBorder-' + props.color}>
      {props.children}
    </div>
  );
}

function Dialog(props) {
  return (
    <FancyBorder color="blue">
      <h1 className="Dialog-title">
        {props.title}
      </h1>
      <p className="Dialog-message">
        {props.message}
      </p>
      {props.children}
    </FancyBorder>
  );
}

class SignUpDialog extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSignUp = this.handleSignUp.bind(this);
    this.state = {login: ''};
  }

  render() {
    return (
      <Dialog title="Mars Exploration Program"
              message="How should we refer to you?">
        <input value={this.state.login}
               onChange={this.handleChange} />
        <button onClick={this.handleSignUp}>
          Sign Me Up!
        </button>
      </Dialog>
    );
  }

  handleChange(e) {
    this.setState({login: e.target.value});
  }

  handleSignUp() {
    alert(`Welcome aboard, ${this.state.login}!`);
  }
}

ReactDOM.render(
  <SignUpDialog />,
  document.getElementById('root')
);

```

=> chia nhỏ theo phân cấp 

* to nhất => css border dùng `props.children` để gọi tất cả thằng con 
    * thằng con => fix 1 số cái như title hoặc mesage để hiển thị dùng props.message để lấy nội dung còn các element con thì lại kêu props.children ra
        * thằng cháu => pass nội dung của title và message vào tự định nghĩa thêm nội dung các e lements bên trong nó 


code từ thằng cháu gọi ngược lên
