xử lý event trong React tương tự trong HTML DOM, có 1 số điểm khác biệt : 
* gọi tên các react event theo kiểu camelCase 
* JSX truyển 1 function như 1 event handler hơn là 1 chuỗi

``` javascript
<button onclick="activateLasers()">
  Activate Lasers
</button>
=>is slightly different in React:
<button onClick={activateLasers}>
  Activate Lasers
</button>
```
sử dụng hàm e.preventDefault() e là sự kiện đc pass vào hàm event handler 
```
function ActionLink() {
  function handleClick(e) {
    e.preventDefault();
    console.log('The link was clicked.');
  }

  return (
    <a href="#" onClick={handleClick}>
      Click me
    </a>
  );
}
```

* thông thường js add event sau khi dom đã đc khởi tạo . nhưng trong react nó đc khai báo trong khởi tạo

## Thận trọng với `this`
để this chỉ trỏ đúng chỗ 
``` javascript
onClick={this.handClick} => gọi thế này thì lúc  click this trỏ về đâu ??????
```
* class ES6 ko dùng bind để gọi hàm ở chế độ default => `this will be undefined when the function is actually called. ` ????
* Khi này this trong `onClick={this.handClick}` sẽ trỏ về hàm:
``` javascript
handleClick (e){
    e.isDefaultPrevented();
    console.log(e)
    this.setState(prevState => ({
      isToggleOn: !prevState.isToggleOn
    }));
  }
```
Nhưng hàm được gọi bởi thằng click nên this là thằng global ( top của brower ) => ko hiểu setState()

* có 2 giải pháp 
    * 1 là gọi call back : onClick={(e) => this.handClick(e)} => lưu ý cách truyển biến
    * 2 là dùng kiểu: 
    
 ``` javascript
  class Bork {
    //Property initializer syntax
    instanceProperty = "bork";
    boundFunction = () => {
      return this.instanceProperty;
    }

    //Static class properties
    static staticProperty = "babelIsCool";
    static staticFunction = function() {
      return Bork.staticProperty;
    }
  }

  let myBork = new Bork;

  //Property initializers are not on the prototype.
  console.log(myBork.__proto__.boundFunction); // > undefined

  //Bound functions are bound to the class instance.
  console.log(myBork.boundFunction.call(undefined)); // > "bork"

  //Static function exists on the class.
  console.log(Bork.staticFunction()); // > "babelIsCool"
```
  Khai báo theo kiểu này gọi đc hàm handleClick thì nó là arrow function nên this be ọke
  
# Passing Arguments to Event Handlers
  Khi muốn khởi tạo 1 loạt các button trong loop:
``` javascript
    <button onClick={(e) => this.deleteRow(id, e)}>Delete Row</button> // e là sự kiện
    <button onClick={this.deleteRow.bind(this, id)}>Delete Row</button>// e được truyển vào auto
```
2 cách là tương đương, chú ý các passing param 
  
  
