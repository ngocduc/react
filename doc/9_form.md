# table of content
`

* [Controlled Components](#controlled-components)
* [Handling Multiple Inputs](#handling-multiple-inputs)

`

* form trong html mặc định submit và chuyển đén trang mới
* react là sự khác biệt! tạo signal page application = react + ajax + micro sevice 

# Controlled Components
* form elements như input select .. thường duy trì 1 state riêng, được khai báo trong constructor và up date bằng this.setState()
```
class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Name:
          <input type="text" value={this.state.value} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}
```

* bằng cách dùng onChange event để setState => lúc onSubmit thì thực hiện xử lý , post put ...
``` javascript
   <select value={this.state.value} onChange={this.handleChange}>
            <option value="grapefruit">Grapefruit</option>
            <option value="lime">Lime</option>
            <option value="coconut">Coconut</option>
            <option value="mango">Mango</option>
    </select>
    <select multiple={true} value={['B', 'C']}>
```
 element select 


# Handling Multiple Inputs

khi cần xử lý nhiều controller input cần xử lý thì add `name` để gọi hàm ` onChange={function} ` và dùng `event.target.name ` để lấy giá trị thay đổi 

``` javascript
class Reservation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isGoing: true,
      numberOfGuests: 2
    };

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <form>
        <label>
          Is going:
          <input
            name="isGoing"
            type="checkbox"
            checked={this.state.isGoing}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Number of guests:
          <input
            name="numberOfGuests"
            type="number"
            value={this.state.numberOfGuests}
            onChange={this.handleInputChange} />
        </label>
      </form>
    );
  }
}

```
```
this.setState({
  [name]: value
});
```
* vì xử lý 2 cái input nên nó thực hiện dùng 1 hàm và toán tử 3 ngôi để bắt => chia thành hai hàm để xứ lý có nên chăng

## xử lý input mà ko cần controler component ???
