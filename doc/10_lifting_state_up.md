## dùng để thay đổi nhiều element cùng lúc

* kỹ thuật lifting state up là cũng giống như push props down dùng để quản lý các element
* để lifting up dùng closure :
    * tạo 1 hàm ở component cha 
    * gọi hàm đố ở component con truyền và tham số để function có thể thay đổi state => thay đổi loạt view

code tự làm:

``` javascript

const tem = {
  c : 'Celsius',
  f : 'Fahrenheit'
}

function convert(scale, value) {
  const input = parseFloat(value);
  if (Number.isNaN(input)) {
    return '';
  }
  return (scale==='c')? ((input - 32) * 5 / 9): ((input * 9 / 5) + 32)

}

class TemperatureInput extends React.Component {
  constructor(props) {
    super(props);
  }
  handleInput = (e) => {
    this.props.calculatorHandle(e.target.value);
  }
  render() {
    const {scale, value} = this.props;
    return (
      <div>
        <label> {tem[scale]} </label>
        <input value={value} onChange={this.handleInput} />
      </div>
    );
  }
}

class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      c:'',
      f:''
    }
  }
  handleInputDc = (textInput) => {
    this.setState({
      c:textInput
    })
    this.setState({
      f: convert('f',textInput)
    })
    console.log(textInput);
  }
  handleInputDf = (textInput) => {
    this.setState({
      c:convert('c',textInput)
    })
    this.setState({
      f: textInput
    })
  }
  render() {
    const {c,f} = this.state;
    return (
      <div>
        <TemperatureInput 
          scale="c"
          value= {c}  
          calculatorHandle={this.handleInputDc} />
        <TemperatureInput 
          scale="f"
          value= {f} 
          calculatorHandle={this.handleInputDf} />
        <p> {c>100? `soi`: `chua soi` }</p>
      </div>
    );
  }
}




ReactDOM.render(
  <Calculator />,
  document.getElementById('root')
);

```